==============
pytest-project
==============

.. image:: https://img.shields.io/pypi/v/troposphere.svg
    :target: https://pypi.python.org/pypi/troposphere

.. image:: https://travis-ci.org/cloudtools/troposphere.png?branch=master
    :target: https://travis-ci.org/cloudtools/troposphere

.. image:: https://img.shields.io/pypi/l/troposphere.svg
    :target: https://opensource.org/licenses/BSD-2-Clause


About
=====

Pytest project using "wallet" example.

Installation
============

xxxxxxx can be installed using the pip distribution system for Python by
issuing:

.. code:: sh

    $ pip install xxxxx

.. code:: sh

    $ pip install xxxxx

Alternatively, you can run use setup.py to install by cloning this repository
and issuing:

.. code:: sh

    $ python setup.py install  # you may need sudo depending on your python installation

Examples
========
Examples from https://semaphoreci.com/community/tutorials/testing-python-applications-with-pytest
